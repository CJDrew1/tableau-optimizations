USE [adv_data]
GO

/****** Object:  UserDefinedFunction [RW].[fn_tx_GetChildNodes]    Script Date: 1/24/2018 3:04:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







/*******************************************************************************
	Author: Alex Ichinoe
	Created: 2016-07-12
	Description:
		For use with Export+ to populate the unit_excl column in RW.eplus_extract
		The unit id in ADVDB.ENTITY.MAIL_CODE6/7/8 is passed through this function
		and a concatenated string of all parent node is created
		Each id is a possible match for a selected unit code in the E+ UI
		If there is a match the entity record is excluded from the resulting output

	Usage: 
		Dance taxonomy id = 71
		SELECT RW.fn_tx_GetChildNodes(17)
		Output: 1,17,71,368

*******************************************************************************/

CREATE FUNCTION [RW].[fn_tx_GetChildNodes] (
	@select_unit int
)

RETURNS nvarchar(max)

AS

BEGIN

	declare @listStr nvarchar(max)

	/************** TESTING ************************
	declare @select_unit int
	set @select_unit = 17
	*/

	select @listStr = coalesce(@listStr + '|', '') + cast(id as nvarchar)
	from ADVRDB.UW_TAXONOMY_EXTRACT
	where lft >= (select lft from ADVRDB.UW_TAXONOMY_EXTRACT where id = @select_unit)
	and rht <= (select rht from ADVRDB.UW_TAXONOMY_EXTRACT where id = @select_unit)

	/************** TESTING ************************
	print @listStr
	*/
    
RETURN  @listStr

END







GO



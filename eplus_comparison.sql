--Exclusions

select top 1000 a.id_number, en.mail_code6, en.MAIL_CODE7, en.MAIL_CODE8, unit_excl, unit_excl_solicit
from rw.eplus_extract a
join ADVDB.ENTITY en on a.id_number = en.ID_NUMBER and (isnull(en.MAIL_CODE6,'') <> '' or isnull(en.MAIL_CODE7,'') <> '' or isnull(en.MAIL_CODE8,'') <> '')
where a.id_number = 0000000552

select top 1000 a.id_number, en.mail_code6, en.MAIL_CODE7, en.MAIL_CODE8, unit_excl
from rw.eplus_extract_CJD_1 a
join ADVDB.ENTITY en on a.id_number = en.ID_NUMBER and (isnull(en.MAIL_CODE6,'') <> '' or isnull(en.MAIL_CODE7,'') <> '' or isnull(en.MAIL_CODE8,'') <> '')
where a.id_number = 0000000552

select top 100 id_number, unit_excl_solicit
from rw.eplus_extract where unit_excl_solicit is not null

--=====================================================================--
--Affils
select top 1000 id_number, affils
from rw.eplus_extract
where id_number = 0000174860

select top 1000 *
from advdb.affiliation
where id_number = 0000174860

select *
from advrdb.uw_taxonomy_extract
order by cast(id as int) desc


declare @select_unit int = 274
select *
from ADVRDB.UW_TAXONOMY_EXTRACT
where lft <= (select lft from ADVRDB.UW_TAXONOMY_EXTRACT where id = @select_unit)
and rht >= (select rht from ADVRDB.UW_TAXONOMY_EXTRACT where id = @select_unit)
USE [adv_data]
GO

/****** Object:  StoredProcedure [RW].[eplus_build_extract]    Script Date: 1/24/2018 2:48:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









/*=============================================
-- Author:		Alex Ichinoe
-- Create date: 2016-04-27
-- Description: Builds an extract for export+

Modified by: 
Namrata Zalewski		4/30		Updated home_label1, pref_label.. logic. It was not working correctly
Alex Ichinoe			7/31/2017	Added pref_state_code to support Marketo Lead object also... It
									doesn't make sense that there is a state code for the other types
									and not the preferred address. (Why has no one asked about this?)
Alex Ichinoe			8/7/2017	1) Adding gender and marital descriptions for both spouses. This is to support 
									Marketo and reduce the number of JOINs
									2) Only include spouse2 in label fields if the entity status for the spouse is "A" or "L". 

Evie Le					8/28/2017	Fix for alumni in affils

Namrata Zalewski	    12/18/2017 updated sub-region logic based on xsequence number
Namrata Zalewski	    01/03/2018 Added hierarchical logic to the lifetime and annual gift clubs
Namrata Zalewski	    01/04/2018 Added run_date column
Alex Ichinoe			01/23/2018 Added references in prep for activation of PREF_JNT_MAIL_NAME 1 & 2. This will require deactivation of the 
									current PREF_JNT_MAIL_NAME field so that field will need to be deactivated.

=============================================*/
ALTER PROCEDURE [RW].[eplus_build_extract_CJD] 
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM sysobjects
				WHERE id = object_id(N'RW.eplus_extract_CJD_1')
				  AND OBJECTPROPERTY(id, N'IsTable') = 1)
	DROP TABLE RW.eplus_extract_CJD_1

CREATE TABLE RW.eplus_extract_CJD_1 (
	id_number CHAR(10) NULL
	,fg_id_number CHAR(10) NULL
	,entity_status NVARCHAR(20) NULL
	,entity_status_desc NVARCHAR(35) NULL
	,person_or_org NVARCHAR(8) NULL
	,entity_record_type NVARCHAR(50) NULL
	,age INT
	,gender NVARCHAR(20) NULL
	,
	--Name & Salutation
	pref_mailing_name NVARCHAR(80) NULL

	--Comment out when PREF_JNT_MAIL_NAME1 & 2 come online
	,pref_jnt_mailing_name NVARCHAR(80) NULL

	,prefix NVARCHAR(25) NULL
	,first_name NVARCHAR(25) NULL
	,middle_name NVARCHAR(25) NULL
	,last_name NVARCHAR(25) NULL
	,prof_suffix NVARCHAR(25) NULL
	,pers_suffix NVARCHAR(25) NULL
	,prospect_manager_name NVARCHAR(80) NULL
	,birth_dt NVARCHAR(10) NULL
	,death_dt NVARCHAR(10) NULL
	,marital_status NVARCHAR(35) NULL
	,sp_id_number CHAR(10) NULL
	,sp_age INT
	,sp_gender NVARCHAR(20) NULL
	,sp_entity_status NVARCHAR(20) NULL
	,sp_entity_status_desc NVARCHAR(35) NULL
	,sp_entity_record_type NVARCHAR(50) NULL
	,sp_pref_mailing_name NVARCHAR(80) NULL
	,sp_prefix NVARCHAR(25) NULL
	,sp_first_name NVARCHAR(25) NULL
	,sp_middle_name NVARCHAR(25) NULL
	,sp_last_name NVARCHAR(25) NULL
	,sp_prof_suffix NVARCHAR(25) NULL
	,sp_pers_suffix NVARCHAR(25) NULL
	,sp_birth_dt NVARCHAR(10) NULL
	,sp_death_dt NVARCHAR(10) NULL
	,sp_email_address NVARCHAR(320) NULL
	,sp_employer_name NVARCHAR(60)
	,sp_job_title NVARCHAR(60)
	,sp_alert NVARCHAR(4000)
	,recognition_name_formal NVARCHAR(120) NULL
	,recognition_name_informal NVARCHAR(120) NULL
	,pref_name_sort NVARCHAR(60) NULL
	,report_name NVARCHAR(60) NULL
	,salutation NVARCHAR(40) NULL
	,salutation_type_desc NVARCHAR(40)
	,salutation_comment NVARCHAR(2000)
	,jnt_salutation NVARCHAR(80) NULL
	,
	--Addresses & Phone Number
	pref_labelname_1 NVARCHAR(80) NULL
	,-- Will always be filled in with the pref_mailing_name
	pref_labelname_2 NVARCHAR(80) NULL
	,-- Only filled in if householded mailings is allowed, otherwise blank
	pref_addr_seq NUMERIC(38, 0) NULL
	,pref_addr_type NVARCHAR(80) NULL
	,pref_addr_type_code NVARCHAR(80) NULL
	,pref_addr_status_code NVARCHAR(40) NULL
	,pref_label1 NVARCHAR(80) NULL
	,pref_label2 NVARCHAR(80) NULL
	,pref_label3 NVARCHAR(80) NULL
	,pref_label4 NVARCHAR(80) NULL
	,pref_label5 NVARCHAR(80) NULL
	,pref_street1 NVARCHAR(80) NULL
	,pref_street2 NVARCHAR(80) NULL
	,pref_street3 NVARCHAR(80) NULL
	,pref_city NVARCHAR(50) NULL
	,pref_state_code NVARCHAR(3) NULL
	,pref_state NVARCHAR(40) NULL
	,pref_zipcode NVARCHAR(30) NULL
	,pref_foreign_cityzip NVARCHAR(75) NULL
	,pref_canadian_zip_code NVARCHAR(7) NULL
	,pref_country_code NVARCHAR(10) NULL
	,pref_country NVARCHAR(40) NULL
	,pref_county_code NVARCHAR(5) NULL
	,pref_county NVARCHAR(40) NULL
	,pref_region_code NVARCHAR(5) NULL
	,pref_region NVARCHAR(40) NULL
	,pref_region_code_sub NVARCHAR(5)
	,pref_region_code_sub_desc NVARCHAR(40)
	,pref_phone_status NVARCHAR(40) NULL
	,pref_phone_area_code NVARCHAR(3) NULL
	,pref_phone_number NVARCHAR(22) NULL
	,pref_formatted_phone NVARCHAR(22) NULL
	,email_address NVARCHAR(320) NULL
	,home_labelname_1 NVARCHAR(80) NULL
	,-- Will always be filled in with the pref_mailing_name
	home_labelname_2 NVARCHAR(80) NULL
	,-- Only filled in if householded mailings is allowed, otherwise blank
	home_addr_seq NUMERIC(38, 0) NULL
	,-- 0 means there is no data
	home_addr_status_code NVARCHAR(40) NULL
	,home_label1 NVARCHAR(80) NULL
	,home_label2 NVARCHAR(80) NULL
	,home_label3 NVARCHAR(80) NULL
	,home_label4 NVARCHAR(80) NULL
	,home_label5 NVARCHAR(80) NULL
	,home_street1 NVARCHAR(80) NULL
	,home_street2 NVARCHAR(80) NULL
	,home_street3 NVARCHAR(80) NULL
	,home_city NVARCHAR(50) NULL
	,home_state_code NVARCHAR(3) NULL
	,home_state NVARCHAR(40) NULL
	,home_zipcode NVARCHAR(30) NULL
	,home_foreign_cityzip NVARCHAR(75) NULL
	,home_canadian_zip_code NVARCHAR(7) NULL
	,home_country_code NVARCHAR(5) NULL
	,home_country NVARCHAR(40) NULL
	,home_county_code NVARCHAR(5) NULL
	,home_county NVARCHAR(40) NULL
	,home_region_code NVARCHAR(5) NULL
	,home_region NVARCHAR(40) NULL
	,home_region_code_sub NVARCHAR(5)
	,home_region_code_sub_desc NVARCHAR(40)
	,home_phone_status NVARCHAR(40) NULL
	,home_phone_area_code NVARCHAR(3) NULL
	,home_phone_number NVARCHAR(22) NULL
	,home_formatted_phone NVARCHAR(22) NULL
	,mobile_phone_area_code NVARCHAR(3)
	,mobile_formatted_phone NVARCHAR(22)
	,mobile_phone_number NVARCHAR(22)
	,mobile_phone_status NVARCHAR(3)
	,bus_labelname_1 NVARCHAR(80) NULL
	,-- Will always be filled in with the pref_mailing_name
	bus_labelname_2 NVARCHAR(80) NULL
	,-- Only filled in if householded mailings is allowed, otherwise blank
	bus_addr_seq NUMERIC(38, 0) NULL
	,-- 0 means there is no data
	bus_addr_status_code NVARCHAR(40) NULL
	,bus_label1 NVARCHAR(80) NULL
	,bus_label2 NVARCHAR(80) NULL
	,bus_label3 NVARCHAR(80) NULL
	,bus_label4 NVARCHAR(80) NULL
	,bus_label5 NVARCHAR(80) NULL
	,bus_street1 NVARCHAR(80) NULL
	,bus_street2 NVARCHAR(80) NULL
	,bus_street3 NVARCHAR(80) NULL
	,bus_city NVARCHAR(50) NULL
	,bus_state_code NVARCHAR(3) NULL
	,bus_state NVARCHAR(40) NULL
	,bus_zipcode NVARCHAR(30) NULL
	,bus_foreign_cityzip NVARCHAR(75) NULL
	,bus_canadian_zip_code NVARCHAR(7) NULL
	,bus_country_code NVARCHAR(5) NULL
	,bus_country NVARCHAR(40) NULL
	,bus_county_code NVARCHAR(5) NULL
	,bus_county NVARCHAR(40) NULL
	,bus_region_code NVARCHAR(5) NULL
	,bus_region NVARCHAR(40) NULL
	,bus_region_code_sub NVARCHAR(5)
	,bus_region_code_sub_desc NVARCHAR(40)
	,bus_phone_status NVARCHAR(40) NULL
	,bus_phone_area_code NVARCHAR(3) NULL
	,bus_phone_number NVARCHAR(22) NULL
	,bus_formatted_phone NVARCHAR(22) NULL
	,
	--Job
	job_title NVARCHAR(60)
	,employer_name NVARCHAR(60)
	,
	--Solicitation
	no_mail CHAR(1) NULL
	,no_mail_solicit CHAR(1) NULL
	,no_email CHAR(1) NULL
	,no_email_solicit CHAR(1) NULL
	,no_phone CHAR(1) NULL
	,no_phone_solicit CHAR(1) NULL
	,recog_excl CHAR(1) NULL
	,solicit_ctrl_code NVARCHAR(1) NULL
	,solicit_ctrl NVARCHAR(35) NULL
	,jnt_mailings_ind CHAR(1) NULL
	,Board_of_Regents NVARCHAR(20) NULL
	,--(A)ctive or (P)ast
	UWAA_Board NVARCHAR(20) NULL
	,--(A)ctive or (P)ast
	UW_Fdn_Board NVARCHAR(20) NULL
	,--(A)ctive or (P)ast
	HSS NVARCHAR(60) NULL
	,unit_excl NVARCHAR(4000) NULL
	,unit_excl_solicit NVARCHAR(4000) NULL
	,unit_incl NVARCHAR(4000) NULL
	,go_green NVARCHAR(1)
	,Receive_Only_From_1 NVARCHAR(255)
	,Receive_Only_From_2 NVARCHAR(255)
	,Receive_Nothing_From_1 NVARCHAR(255)
	,Receive_Nothing_From_2 NVARCHAR(255)
	,Receive_Nothing_From_3 NVARCHAR(255)
	,No_Solicitations_From_1 NVARCHAR(255)
	,No_Solicitations_From_2 NVARCHAR(255)
	,
	--Membership
	gift_club_annual NVARCHAR(80) NULL
	,gift_club_lifetime NVARCHAR(80) NULL
	,uwaa_member NVARCHAR(35) NULL
	,uwra_member NVARCHAR(35) NULL
	,burke_member NVARCHAR(35) NULL
	,pharm_member NVARCHAR(35) NULL
	,
	--Degree
	pref_class_year NVARCHAR(4)
	,pref_school NVARCHAR(40)
	,
	--Prospect Description
	uw_classification_desc NVARCHAR(40)
	,uw_contacter_name NVARCHAR(60)
	,uw_fundraisers_assigned NVARCHAR(1000)
	,uw_group_description NVARCHAR(40)
	,--HSS
	uw_last_contact_date DATE
	,uw_last_contact_desc NVARCHAR(120)
	,uw_last_contact_type_desc NVARCHAR(40)
	,uw_last_personal_contact_date NVARCHAR(10)
	,uw_last_personal_contact_desc NVARCHAR(120)
	,uw_last_personal_contacter_name NVARCHAR(60)
	,uw_last_sig_contact_date NVARCHAR(10)
	,uw_last_sig_contact_desc NVARCHAR(120)
	,uw_last_sig_contact_name NVARCHAR(60)
	,uw_last_sig_contact_type_desc NVARCHAR(40)
	,major_prospect_description NVARCHAR(60)
	,programs NTEXT
	,prospect_id NUMERIC(10, 0)
	,prospect_name NVARCHAR(60)
	,prospect_name_sort NVARCHAR(60)
	,rating_description NVARCHAR(40)
	,
	--relationship_manager   nvarchar(255),
	engagement_score_house NUMERIC(10)
	,henry_suzzallo_society NVARCHAR(20)
	,henry_suzzallo_society_desc NVARCHAR(60)
	,FGModelRanking INT
	,FGModelRating VARCHAR(50)
	,prospect_region_code NVARCHAR(5)
	,prospect_region_code_desc NVARCHAR(40)
	,prospect_region_code_sub NVARCHAR(5)
	,prospect_region_code_sub_desc NVARCHAR(40)
	,
	--Allocation & Amount
	uw_lifetime_fc_amount NUMERIC(38, 0) NULL
	,lifetime_unit_amount NUMERIC(38, 0) NULL
	,uw_last_gift_date NVARCHAR(10) NULL
	,uw_last_gift_alloc_code NVARCHAR(300) NULL
	,uw_last_gift_allocation NVARCHAR(255) NULL
	,uw_last_gift_amount NUMERIC(38, 0) NULL
	,uw_largest_gift_date NVARCHAR(10) NULL
	,uw_largest_gift_alloc_code NVARCHAR(300) NULL
	,uw_largest_gift_allocation NVARCHAR(255) NULL
	,uw_largest_gift_amount NUMERIC(38, 0) NULL
	,uw_first_gift_date NVARCHAR(10) NULL
	,uw_first_gift_alloc_code NVARCHAR(300) NULL
	,uw_first_gift_allocation NVARCHAR(255) NULL
	,uw_first_gift_amount NUMERIC(38, 0) NULL
	,uw_years_of_consec_giving INT NULL
	,uw_year_of_giving_total INT NULL
	,uw_open_pledge_total NUMERIC(38, 0)
	,affils NVARCHAR(max) NULL
	,alert NVARCHAR(4000)
	,run_date datetime

	)



--print 'Begin insert of base population (' + cast(getdate() as nvarchar) + ')'

/***************************************************************************************************
    Initial population
****************************************************************************************************/

	insert into RW.eplus_extract_CJD_1 (
	  id_number
	, fg_id_number
	, entity_record_type
	, entity_status
	, entity_status_desc
	, report_name
	, pref_name_sort
	, pref_mailing_name

	--Comment out when PREF_JNT_MAIL_NAME1 & 2 come online
	, pref_jnt_mailing_name

	, pref_labelname_1
	, home_labelname_1
	, bus_labelname_1
	, salutation
	, jnt_salutation
	, person_or_org
	, solicit_ctrl_code
	, jnt_mailings_ind
	, prefix
	, FIRST_NAME
	, MIDDLE_NAME
	, LAST_NAME
	, PERS_SUFFIX
	, PROF_SUFFIX
	, PROSPECT_MANAGER_NAME
	, sp_id_number
	, sp_pref_mailing_name
	, sp_entity_status
	, sp_entity_record_type
	, uw_lifetime_fc_amount
	, job_title
    , employer_name
    , pref_class_year
    , engagement_score_house
    , pref_school 
    , pref_addr_type 
	)
	select
	--top 5000  -- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< DEBUG
	  id_number
	, fg_id_number
	, entity_record_type
	, ENTITY_STATUS
	, c.adv_short_desc			--entity status
	, report_name
	, PREF_NAME_SORT
	, pref_mailing_name
	
	--Comment out when PREF_JNT_MAIL_NAME1 & 2 come online, pref_mailing_name
	, PREF_JNT_MAIL_NAME

	--UN-Comment out when PREF_JNT_MAIL_NAME1 & 2 come online, pref_mailing_name
	--, PREF_JNT_MAIL_NAME1			--pref_labelname_1
	--, PREF_JNT_MAIL_NAME1			--home_labelname_1
	--, PREF_JNT_MAIL_NAME1			--bus_labelname_1
	--, PREF_JNT_MAIL_NAME2			--pref_labelname_2
	--, PREF_JNT_MAIL_NAME2			--home_labelname_2
	--, PREF_JNT_MAIL_NAME2			--bus_labelname_2

	, PREF_MAILING_NAME			--pref_labelname_1
	, PREF_MAILING_NAME			--home_labelname_1
	, PREF_MAILING_NAME			--bus_labelname_1
	, salutation
	, jnt_salutation
	, person_or_org
	, solicit_ctrl_code
	, JNT_MAILINGS_IND
	, prefix
	, FIRST_NAME
	, MIDDLE_NAME
	, LAST_NAME
	, PERS_SUFFIX
	, PROF_SUFFIX
	, PROSPECT_MANAGER_NAME
	, SPOUSE_ID_NUMBER
	, SPOUSE_NAME
	, SPOUSE_ENTITY_STATUS
	, SPOUSE_ENTITY_TYPE
	, LIFETIME_FC_AMOUNT
	, JOB_TITLE
	, EMPLOYER_NAME
	, PREF_CLASS_YEAR
	, ENGAGEMENT_SCORE_HOUSE
	, b.adv_short_desc --rg.fn_degschool(pref_school_code) 
	, pref_addr_type_code
	from ADVRDB.UW_BIO_EXTRACT a	--1:13
	left join advdb.zz_adv_table b ON b.adv_table_code = a.pref_school_code and b.adv_table_type = 'AF'	--34s
	left join advdb.zz_adv_table c ON c.adv_table_code = a.entity_status and c.adv_table_type = 'AB'	
	where ENTITY_STATUS in ('A','D','L','R')

--print 'End insert of base population (' + cast(getdate() as nvarchar) + ')'	

/***************************************************************************************************
    Index
****************************************************************************************************/

	--IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('RW.eplus_extract_CJD_1') AND NAME in ('eplus_x_id_number'))
	--	DROP INDEX eplus_x_id_number ON RW.eplus_extract_CJD_1
	--	CREATE UNIQUE INDEX eplus_x_id_number on RW.eplus_extract_CJD_1(id_number, sp_id_number)

/***************************************************************************************************
    Updates
****************************************************************************************************/

--print 'Update marital status (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.marital_status = z.ADV_SHORT_DESC --select top 100 *
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_BIO_EXTRACT b on a.id_number = b.ID_NUMBER
		join ADVDB.ZZ_ADV_TABLE z on b.MARITAL_STATUS_CODE = z.ADV_TABLE_CODE
			and z.ADV_TABLE_TYPE = 'BN'
			
--print 'Update gender (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.gender = z.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_BIO_EXTRACT b on a.id_number = b.ID_NUMBER
		join ADVDB.ZZ_ADV_TABLE z on b.GENDER = z.ADV_TABLE_CODE
			and z.ADV_TABLE_TYPE = 'GY'

--print 'Update Spouse gender (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.sp_gender = z.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_BIO_EXTRACT b on a.sp_id_number = b.ID_NUMBER
		join ADVDB.ZZ_ADV_TABLE z on b.GENDER = z.ADV_TABLE_CODE
			and z.ADV_TABLE_TYPE = 'GY'

--print 'Update Spouse entity status (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.sp_entity_status_desc = b.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ZZ_ADV_TABLE b on a.sp_entity_status = b.ADV_TABLE_CODE
			and b.ADV_TABLE_TYPE = 'AB'

--print 'Update birth and death dates (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.birth_dt = case
                        when b.BIRTH_DT is null then ''
						when b.BIRTH_DT = '00000000' then ''
                    else
						SUBSTRING(b.BIRTH_DT, 5, 2) + '/' + RIGHT(b.BIRTH_DT, 2) + '/' + left(b.BIRTH_DT, 4)
                    end
		, a.death_dt = case
                        when b.DEATH_DT is null then ''
						when b.DEATH_DT = '00000000' then ''
                    else
						SUBSTRING(b.DEATH_DT, 5, 2) + '/' + RIGHT(b.DEATH_DT, 2) + '/' + left(b.DEATH_DT, 4)
                    end
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ENTITY b on a.id_number = b.id_number

	
   CREATE TABLE #birth_date (
          id_number [char] (10) NULL,
          birth_dt [varchar] (8) NULL ,
          birth_year [varchar] (4) NULL ,
          current_fy [varchar] (4) NULL )
          ON [PRIMARY]

	declare @current_fy int = (select dbo.fn_GetCurrentFY(getdate()))

   INSERT INTO #birth_date (
          id_number,
          birth_dt ,
          current_fy )
   SELECT id_number,
          birth_dt ,
          @current_fy
     FROM ADVDB.entity
    WHERE birth_dt > '18000101'

    UPDATE #birth_date
       SET birth_year = 
           CASE 
             WHEN SUBSTRING(birth_dt,5,2) > '06' THEN left(birth_dt,4)+ 1
             ELSE left(birth_dt,4)
           END

    UPDATE ep
       SET ep.age =  CONVERT(INT, current_fy) - CONVERT(INT, birth_year)
      FROM rw.eplus_extract_CJD_1 ep
     INNER JOIN #birth_date db
        ON ep.id_number = db.id_number

	DROP TABLE #birth_date	

--print 'Update spouse birth and death dates (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.sp_birth_dt = case
                        when b.BIRTH_DT is null then ''
						when b.BIRTH_DT = '00000000' then ''
                    else
						SUBSTRING(b.BIRTH_DT, 5, 2) + '/' + RIGHT(b.BIRTH_DT, 2) + '/' + left(b.BIRTH_DT, 4)
                    end
		, a.sp_death_dt = case
                        when b.DEATH_DT is null then ''
						when b.DEATH_DT = '00000000' then ''
                    else
						SUBSTRING(b.DEATH_DT, 5, 2) + '/' + RIGHT(b.DEATH_DT, 2) + '/' + left(b.DEATH_DT, 4)
                    end
		, a.sp_first_name = b.FIRST_NAME
		, a.sp_middle_name = b.MIDDLE_NAME
		, a.sp_last_name = b.LAST_NAME
		, a.sp_pers_suffix = b.PERS_SUFFIX
		, a.sp_prof_suffix = b.PROF_SUFFIX
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ENTITY b on a.sp_id_number = b.id_number

--print 'Update recognition names (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.recognition_name_informal = isnull(b.xcomment,'')
	from RW.eplus_extract_CJD_1 a
		join advdb.name b on a.id_number = b.ID_NUMBER
			and b.NAME_TYPE_CODE = 'RI'

	update a
	set a.recognition_name_formal = isnull(b.xcomment,'')
	from RW.eplus_extract_CJD_1 a
		join advdb.name b on a.id_number = b.ID_NUMBER
			and b.NAME_TYPE_CODE = 'RF'

--print 'Update addresses (' + cast(getdate() as nvarchar) + ')'	

	update a
	set
	  a.pref_label1 = b.PREF_LABEL1
	, a.pref_label2 = b.PREF_LABEL2
	, a.pref_label3 = b.PREF_LABEL3
	, a.pref_label4 = b.PREF_LABEL4
	, a.pref_label5 = b.PREF_LABEL5
	, a.pref_street1 = b.PREF_STREET1
	, a.pref_street2 = b.PREF_STREET2
	, a.pref_street3 = b.PREF_STREET3
	, a.pref_city = b.PREF_CITY
	, a.pref_state_code = b.PREF_STATE
	, a.pref_zipcode = b.PREF_ZIPCODE
	, a.pref_foreign_cityzip = b.PREF_FOREIGN_CITYZIP
	, a.pref_canadian_zip_code = b.PREF_CANADIAN_ZIP_CODE
	, a.pref_country = b.PREF_COUNTRY
	, a.pref_country_code = b.PREF_COUNTRY_CODE
	, a.pref_county = b.PREF_COUNTY
	, a.pref_county_code = b.PREF_COUNTY_CODE
	, a.pref_addr_seq = b.PREF_ADDR_SEQ
	, a.pref_addr_status_code = p.ADV_SHORT_DESC
	, a.pref_addr_type_code = b.PREF_ADDR_TYPE_CODE
	, a.pref_phone_status = pt.ADV_SHORT_DESC
	, a.pref_phone_area_code = b.PREF_PHONE_AREA_CODE
	, a.pref_phone_number = b.PREF_PHONE_NUMBER
	, a.pref_formatted_phone = b.PREF_FORMATTED_PHONE
	, a.email_address = b.EMAIL_ADDRESS
	, a.home_label1 = b.HOME_LABEL1
	, a.home_label2 = b.HOME_LABEL2
	, a.home_label3 = b.HOME_LABEL3
	, a.home_label4 = b.HOME_LABEL4
	, a.home_label5 = b.HOME_LABEL5
	, a.home_city = b.HOME_CITY
	, a.home_state_code = b.HOME_STATE
	, a.home_zipcode = b.HOME_ZIPCODE
	, a.home_phone_area_code = b.HOME_PHONE_AREA_CODE
	, a.home_phone_number = b.HOME_PHONE_NUMBER
	, a.home_formatted_phone = b.HOME_FORMATTED_PHONE
	, a.home_phone_status = ht.ADV_SHORT_DESC
	, a.home_addr_status_code = h.ADV_SHORT_DESC
	, a.home_addr_seq = b.HOME_ADDR_SEQ
	, a.bus_addr_seq = b.BUS_ADDR_SEQ
	, a.bus_label1 = b.BUS_LABEL1
	, a.bus_label2 = b.BUS_LABEL2
	, a.bus_label3 = b.BUS_LABEL3
	, a.bus_label4 = b.BUS_LABEL4
	, a.bus_label5 = b.BUS_LABEL5
	, a.bus_phone_area_code = b.BUS_PHONE_AREA_CODE
	, a.bus_phone_number = b.BUS_PHONE_NUMBER
	, a.bus_formatted_phone = b.BUS_FORMATTED_PHONE
	, a.bus_phone_status = bt.ADV_SHORT_DESC
	, a.bus_addr_status_code = bu.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_ADDRESS_EXTRACT b on a.id_number = b.ID_NUMBER 
		left outer join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'AJ') p on b.pref_addr_status_code = p.ADV_TABLE_CODE
		left outer Join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'AJ') h on b.home_addr_status_code = h.ADV_TABLE_CODE
		left outer join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'AJ') bu on b.bus_addr_status_code = bu.ADV_TABLE_CODE
		left outer join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'PS') pt on b.pref_phone_status = pt.ADV_TABLE_CODE
		left outer join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'PS') ht on b.home_phone_status = ht.ADV_TABLE_CODE
		left outer join (select * from advdb.ZZ_ADV_TABLE where ADV_TABLE_type = 'PS') bt on b.bus_phone_status = bt.ADV_TABLE_CODE

/***************************************************************************************************
    Indexes
****************************************************************************************************/

	--create nonclustered index eplus_x_pref_addrseq on RW.eplus_extract_CJD_1(id_number, pref_addr_seq)
	--create nonclustered index eplus_x_home_addrseq on RW.eplus_extract_CJD_1(id_number, home_addr_seq)
	--create nonclustered index eplus_x_bus_addrseq on RW.eplus_extract_CJD_1(id_number, bus_addr_seq)

/***************************************************************************************************
    Updates
****************************************************************************************************/

	update a
	set a.pref_region_code = d.REGION_CODE
		, a.pref_region = d.REGION_CODE_DESc
		, a.pref_state = d.STATE_CODE_DESC
		,pref_region_code_sub = d.region_code_sub
	  , pref_region_code_sub_desc = d.region_code_sub_desc
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_ADDRESS d on a.id_number = d.ID_NUMBER
			and a.pref_addr_seq = d.XSEQUENCE

	update a
	set a.home_street1 = d.STREET1
		, a.home_street2 = d.STREET2
		, a.home_street3 = d.STREET3
		, a.home_foreign_cityzip = d.FOREIGN_CITYZIP
		, a.home_country_code = d.COUNTRY_CODE
		, a.home_county_code = d.COUNTY_CODE
	from RW.eplus_extract_CJD_1 a --select top 10000 d.* from rw.eplus_extract a
		join ADVDB.ADDRESS d on a.id_number = d.ID_NUMBER
			and a.home_addr_seq = d.XSEQUENCE

	update a
	set a.bus_street1 = d.STREET1
		, a.bus_street2 = d.STREET2
		, a.bus_street3 = d.STREET3
		, a.bus_city = d.CITY
		, a.bus_state_code = d.STATE_CODE
		, a.bus_zipcode = d.ZIPCODE
		, a.bus_foreign_cityzip = d.FOREIGN_CITYZIP
		, a.bus_country_code = d.COUNTRY_CODE
		, a.bus_county_code = d.COUNTY_CODE
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ADDRESS d on a.id_number = d.ID_NUMBER
			and a.bus_addr_seq = d.XSEQUENCE

	update a
	set a.home_region_code = d.REGION_CODE
		, a.home_region = d.REGION_CODE_DESC
		, a.home_canadian_zip_code = d.CANADIAN_ZIP_CODE
		, a.home_state = d.STATE_CODE_DESC
		, a.home_country = d.COUNTRY_CODE_DESC
		, a.home_county = d.COUNTY_CODE_DESC
		,home_region_code_sub = d.region_code_sub
		,home_region_code_sub_desc = d.region_code_sub_desc
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_ADDRESS d on a.id_number = d.ID_NUMBER
			and a.home_addr_seq = d.XSEQUENCE

	update a
	set a.bus_region_code = d.REGION_CODE
		, a.bus_region = d.REGION_CODE_DESC
		, a.bus_canadian_zip_code = d.CANADIAN_ZIP_CODE
		, a.bus_state = d.STATE_CODE_DESC
		, a.bus_country = d.COUNTRY_CODE_DESC
		, a.bus_county = d.COUNTY_CODE_DESC
		,bus_region_code_sub = d.region_code_sub
		,bus_region_code_sub_desc = d.region_code_sub_desc
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_ADDRESS d on a.id_number = d.ID_NUMBER
			and a.bus_addr_seq = d.XSEQUENCE

	update a
	set a.sp_email_address = c.EMAIL_ADDRESS
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_BIO_EXTRACT b on a.id_number = b.ID_NUMBER
		join ADVRDB.UW_ADDRESS_EXTRACT c on b.SPOUSE_ID_NUMBER = c.ID_NUMBER
	where isnull(b.SPOUSE_ID_NUMBER,'') <> ''

--print 'Update solicitation control (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.solicit_ctrl = sc.ADV_SHORT_DESC --select a.*
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ZZ_ADV_TABLE sc on a.solicit_ctrl_code = sc.ADV_TABLE_CODE
			and sc.ADV_TABLE_TYPE = 'BE'

--print 'Update board of regents (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.Board_of_Regents = CASE WHEN b.COMMITTEE_STATUS_CODE = 'A' THEN 'Active'
								  WHEN b.COMMITTEE_STATUS_CODE = 'P' THEN 'Past'
							 END
	from RW.eplus_extract_CJD_1 a
		join ADVDB.COMMITTEE b on a.id_number = b.ID_NUMBER
			and b.COMMITTEE_CODE = 'UW001'		--UW Board of Regents
			and b.COMMITTEE_STATUS_CODE in ('A','P')

--print 'Update UWAA board (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.UWAA_Board = CASE WHEN b.COMMITTEE_STATUS_CODE = 'A' THEN 'Active'
					        WHEN b.COMMITTEE_STATUS_CODE = 'P' THEN 'Past'
							 END
	from RW.eplus_extract_CJD_1 a
		join ADVDB.COMMITTEE b on a.id_number = b.ID_NUMBER
			and b.COMMITTEE_CODE = 'AA300'		--UWAA Board of Trustees
			and b.COMMITTEE_STATUS_CODE in ('A','P')

--print 'Update Foundation board (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.UW_Fdn_Board = CASE WHEN b.COMMITTEE_STATUS_CODE = 'A' THEN 'Active'
							  WHEN b.COMMITTEE_STATUS_CODE = 'P' THEN 'Past'
							 END
	from RW.eplus_extract_CJD_1 a
		join ADVDB.COMMITTEE b on a.id_number = b.ID_NUMBER
			and b.COMMITTEE_CODE = 'UWFB'		--UW Foundation Board
			and b.COMMITTEE_STATUS_CODE in ('A','P')

/***************************************************************************************************
    HSS - for entities who are not prospects
****************************************************************************************************/

--print 'Update HSS (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.HSS = case gift_club_code when 'PHA' then 'Henry Suzzallo Society, No Listing' when 'PHS' then 'Henry Suzzallo Society' end 
	from RW.eplus_extract_CJD_1 a join 
	     (select a.GIFT_CLUB_ID_NUMBER, 
		        gift_club_code, 
                  ROW_NUMBER() over ( partition by a.GIFT_CLUB_ID_NUMBER order by a.gift_club_code) ro_no
            from advdb.GIFT_CLUBS a
            where a.gift_club_code in ('PHS','PHA')
            and a.GIFT_CLUB_STATUS = 'A'
            and a.GIFT_CLUB_SEQUENCE = (select max(b.gift_club_sequence)
								from advdb.GIFT_CLUBS b
								where a.GIFT_CLUB_ID_NUMBER = b.GIFT_CLUB_ID_NUMBER
								   and a.gift_club_code = b.gift_club_code
					   )) c 
	on (a.id_number = c.GIFT_CLUB_ID_NUMBER)
     where c.ro_no = 1;

--print 'Update Annual Gift Clubs (' + cast(getdate() as nvarchar) + ')'	

    WITH club_hier_ann
     AS (SELECT DISTINCT
                x.GIFT_CLUB_ID_NUMBER id_number,
                x.GIFT_CLUB_CODE,
                y.CLUB_DESCRIPTION,
                y.HIERARCHY_ORDER,
                ROW_NUMBER() OVER(PARTITION BY x.GIFT_CLUB_ID_NUMBER ORDER BY y.HIERARCHY_ORDER,
                                                                              x.GIFT_CLUB_CODE) rk
         FROM ADVDB.GIFT_CLUBS x
              JOIN ADVDB.GIFT_CLUB_TABLE y ON x.GIFT_CLUB_CODE = y.CLUB_CODE
         WHERE x.GIFT_CLUB_CODE IN ('UW6','UW7','UW8','UW9','UWA','UWB','UWC','UWD','UWE','UWF')
	    and GIFT_CLUB_END_DATE = dbo.fn_GetCurrentFY(getdate()) + '0630'
	   -- and GIFT_CLUB_ID_NUMBER = '0000742973'
	    )
			
	update a
	set a.gift_club_annual = b.CLUB_DESCRIPTION
	from RW.eplus_extract_CJD_1 a
		join club_hier_ann b on a.id_number = b.id_number and rk =1;
   -- where a.ID_NUMBER = '0000742973'


--print 'Update lifetime gift clubs (' + cast(getdate() as nvarchar) + ')'	

   WITH club_hier
     AS (SELECT DISTINCT
                x.GIFT_CLUB_ID_NUMBER id_number,
                x.GIFT_CLUB_CODE,
                y.CLUB_DESCRIPTION,
                y.HIERARCHY_ORDER,
                ROW_NUMBER() OVER(PARTITION BY x.GIFT_CLUB_ID_NUMBER ORDER BY y.HIERARCHY_ORDER,
                                                                              x.GIFT_CLUB_CODE) rk
         FROM ADVDB.GIFT_CLUBS x
              JOIN ADVDB.GIFT_CLUB_TABLE y ON x.GIFT_CLUB_CODE = y.CLUB_CODE
         WHERE x.GIFT_CLUB_CODE IN('WBF', 'VBA', 'XMD', 'AMD', 'PRL', 'APL', 'DPL', 'ADP', 'RL', 'ARL')
             -- AND gift_club_id_number = '0000043656'
		  )

     UPDATE a
       SET
           a.gift_club_lifetime = b.CLUB_DESCRIPTION --SELECT *
     FROM RW.eplus_extract_CJD_1 a
          JOIN club_hier b ON a.id_number = b.id_number
                                     AND rk = 1
    --where a.id_number = '0000043656'


	--update a
	--set a.gift_club_lifetime = t.CLUB_DESCRIPTION
	--from RW.eplus_extract_CJD_1 a
	--	join ADVDB.GIFT_CLUBS b on a.id_number = b.GIFT_CLUB_ID_NUMBER
	--		and GIFT_CLUB_CODE in ('ULF','WBF','VBA','XMD','AMD','PRL','APL','ARL','RL')
	--		and GIFT_CLUB_STATUS = 'A'
	--	left join ADVDB.GIFT_CLUB_TABLE t on b.GIFT_CLUB_CODE = t.CLUB_CODE

--print 'Update UWAA member (' + cast(getdate() as nvarchar) + ')'	
		
	update a
	set a.uwaa_member = zz.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_MEMBERSHIP_EXTRACT_ALL b on a.id_number = b.ID_NUMBER
			and b.memb_unit_code = 'UA'
		join ADVDB.ZZ_ADV_TABLE zz on b.MEMB_CURRENT_STATUS_CODE = zz.ADV_TABLE_CODE
			and zz.ADV_TABLE_TYPE = 'MS'

--print 'Update UWRA Member (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.uwra_member = zz.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_MEMBERSHIP_EXTRACT_ALL b on a.id_number = b.ID_NUMBER
			and b.memb_unit_code = 'UR'
		join ADVDB.ZZ_ADV_TABLE zz on b.MEMB_CURRENT_STATUS_CODE = zz.ADV_TABLE_CODE
			and zz.ADV_TABLE_TYPE = 'MS'

--print 'Update Pharm member (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.pharm_member = zz.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_MEMBERSHIP_EXTRACT_ALL b on a.id_number = b.ID_NUMBER
			and b.memb_unit_code = 'PH'
		join ADVDB.ZZ_ADV_TABLE zz on b.MEMB_CURRENT_STATUS_CODE = zz.ADV_TABLE_CODE
			and zz.ADV_TABLE_TYPE = 'MS'

--print 'Update Burke member (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.burke_member = zz.ADV_SHORT_DESC
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_MEMBERSHIP_EXTRACT_ALL b on a.id_number = b.ID_NUMBER
			and b.memb_unit_code = 'BK'
		join ADVDB.ZZ_ADV_TABLE zz on b.MEMB_CURRENT_STATUS_CODE = zz.ADV_TABLE_CODE
			and zz.ADV_TABLE_TYPE = 'MS'

--print 'Update UW-Level GBUX data (' + cast(getdate() as nvarchar) + ')'	

	update a
	set a.uw_first_gift_alloc_code = b.first_gift_alloc_desc + ' (' + b.first_gift_alloc + ' )'
		, a.uw_first_gift_amount = b.FIRST_GIFT_AMOUNT
		, a.uw_first_gift_date = convert(nvarchar(10), b.FIRST_GIFT_DATE, 101)
		, a.uw_largest_gift_alloc_code = b.largest_gift_alloc_desc + ' (' + b.largest_gift_alloc + ' )'
		, a.uw_largest_gift_amount = b.LARGEST_GIFT_AMOUNT
		, a.uw_largest_gift_date = convert(nvarchar(10), b.LARGEST_GIFT_DATE, 101)
		, a.uw_last_gift_alloc_code = b.last_gift_alloc_desc + ' (' + b.last_gift_alloc + ' )'
		, a.uw_last_gift_amount = b.LAST_GIFT_AMOUNT
		, a.uw_last_gift_date = convert(nvarchar(10), b.LAST_GIFT_DATE, 101)
		, a.uw_years_of_consec_giving = b.YEARS_OF_CONSEC_GIVING
		, a.uw_year_of_giving_total = b.YEARS_OF_GIVING_TOTAL
		,a.uw_open_pledge_total = b.open_pledge_total
	from RW.eplus_extract_CJD_1 a
		join ADVRDB.UW_GIFT_BY_UNIT_EXTRACT_HOUSEHOLD b on (a.fg_id_number = b.ID_NUMBER
			and b.UNIT = 1) 

--print 'Update Contact Prefs (' + cast(getdate() as nvarchar) + ')'	
			
	--Contact Preferences
	update a
	set a.no_mail = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE1 = 'NA'
	or b.RECORD_STATUS_CODE in ('L', 'R', 'D')

	update a
	set a.no_mail_solicit = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE1 in ('NA','NS')
	or b.solicit_ctrl_code IN ('1','3','9')
	or b.RECORD_STATUS_CODE in ('L', 'R', 'D')

	update a
	set a.no_email = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE3 = 'NA'
	or b.RECORD_STATUS_CODE in ('R','D')

	update a
	set a.no_email_solicit = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE3 in ('NA','NS')
	or b.solicit_ctrl_code IN ('1','3','9')
	or b.RECORD_STATUS_CODE in ('R','D')

	update a
	set a.no_phone = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE2 = 'NA'
	or b.RECORD_STATUS_CODE in ('R', 'D')

	update a
	set a.no_phone_solicit = 'Y'
	from RW.eplus_extract_CJD_1 a
		join advdb.entity b on a.id_number = b.ID_NUMBER
	where b.MAIL_CODE2 in ('NA','NS')
	or b.solicit_ctrl_code IN ('1','3','9')
	or b.RECORD_STATUS_CODE in ('R', 'D')

	update a
	set	a.recog_excl = 'Y'
	from RW.eplus_extract_CJD_1 a
		join ADVDB.MAILING_LIST b on a.id_number = b.ID_NUMBER
			and b.MAIL_LIST_CODE = 'CD020'
			and b.MAIL_LIST_STATUS_CODE = 'A'

	--Comment out when PREF_JNT_MAIL_NAME1 & 2 come online, pref_mailing_name
	update a
	set a.pref_labelname_2 = a.sp_pref_mailing_name
		, a.home_labelname_2 = a.sp_pref_mailing_name
		, a.bus_labelname_2 = a.sp_pref_mailing_name
	from RW.eplus_extract_CJD_1 a
	where jnt_mailings_ind = 'Y'
	and isnull(a.sp_id_number, '') <> ''
	and a.sp_entity_status <> 'R'
	
      update a 
	  set  a.mobile_phone_area_code = b.area_code,
	       a.mobile_phone_number = b.telephone_number,
            a.mobile_formatted_phone = case when isnull(b.area_code,'') <> '' and isnull(b.telephone_number,'') <> ''  then '(' +  b.area_code + ')' + substring(b.telephone_number,1,3) + '-' +  substring(b.telephone_number,4,len(b.telephone_number)) else b.telephone_number end,
		  a.mobile_phone_status = b.telephone_status_code
	 from  RW.eplus_extract_CJD_1 a join
	      [ADVDB].[TELEPHONE] b on (a.id_number = b.id_number)
	where b.telephone_type_code = 'N' and
		 TELEPHONE_STATUS_CODE = 'A' and
		 b.XSEQUENCE = (select max(z.xsequence)
                  from  [ADVDB].[TELEPHONE] z 
			   where b.id_number = z.id_number
			    and b.telephone_type_code = z.telephone_type_code
			    and b.telephone_status_code = z.telephone_status_code
			 )

--print 'Update subregions (' + cast(getdate() as nvarchar) + ')'	

	/*--Sub Regions
	update a
	set bus_region_code_sub = b.region_code_sub,
		bus_region_code_sub_desc = b.region_code_sub_desc
	from RW.eplus_extract_CJD_1 a join
		 advrdb.uw_address b on (a.id_number = b.id_number and
								 b.addr_type_code = 'B' and
								 addr_status_code = 'A')
	
	update a
	set home_region_code_sub = b.region_code_sub,
		home_region_code_sub_desc = b.region_code_sub_desc
	from RW.eplus_extract_CJD_1 a join
		 advrdb.uw_address b on (a.id_number = b.id_number and
								 b.addr_type_code = 'H' and
								 b.addr_status_code = 'A')

	update a
	set pref_region_code_sub = b.region_code_sub
	  , pref_region_code_sub_desc = b.region_code_sub_desc
    from RW.eplus_extract_CJD_1 a join
		 advrdb.uw_address b on (a.id_number = b.id_number and
								 b.addr_pref_ind = 'Y' and
								 b.addr_status_code = 'A')
*/
--print 'Update unit-specific contact prefs (' + cast(getdate() as nvarchar) + ')'	

	--Solicitation
    update a
     set Receive_Nothing_From_1 =	c.unit_description --mail_code6
	    ,Receive_Nothing_From_2 =	d.unit_description -- mail_code7
        ,Receive_Nothing_From_3 =	e.[unit_description] --mail_code8
        ,Receive_Only_From_1 =		f.unit_description --mail_code4
        ,Receive_Only_From_2 =		g.[unit_description] --mail_code5
        ,No_Solicitations_From_1 =	h.[unit_description] --mail_code9
        ,No_Solicitations_From_2 =	j.[unit_description] --mail_code10
	 from RW.eplus_extract_CJD_1 a join
     advdb.entity  b on (a.id_number = b.id_number) left outer join 
	advrdb.UW_Taxonomy c on (isnull(b.mail_code6,0) = c.id) left outer join 
	advrdb.UW_Taxonomy d on (isnull(b.mail_code7,0) = d.id) left outer join
	advrdb.UW_Taxonomy e on (isnull(b.mail_code8,0) = e.id) left outer join
	advrdb.UW_Taxonomy f on (isnull(b.mail_code4,0) = f.id) left outer join
	advrdb.UW_Taxonomy g on (isnull(b.mail_code5,0) = g.id) left outer join 
	advrdb.UW_Taxonomy h on (isnull(b.mail_code9,0) = h.id) left outer join
	advrdb.UW_Taxonomy j on (isnull(b.mail_code10,0) = j.id) 

--print 'Update go green (' + cast(getdate() as nvarchar) + ')'	

	  update a
      set a.go_green = 'Y'
      from RW.eplus_extract_CJD_1 a join
           advdb.mailing_list  b on (a.id_number = b.id_number and
                                    b.mail_list_code = 'CD017')

--print 'Update FG Model (' + cast(getdate() as nvarchar) + ')'	

       --FGModelranking
	   update a
       set a.FGModelRanking = b.FGModelRanking,
           a.FGModelRating = b.FGModelRating
       from RW.eplus_extract_CJD_1 a join
             ar.model_master b on (a.id_number = b.id_number and
							       b.active_ind = 'Y' and 
							       b.taxonomy_code = '1')

--print 'Update spouse info (' + cast(getdate() as nvarchar) + ')'	

        --Spouse information
		update a
		set a.sp_age = case when isdate(sp_birth_dt) > 0 then  convert(int, year(getdate())) -  convert(int, year(convert(date, sp_birth_dt))) end 
		from  RW.eplus_extract_CJD_1 a

		update a
		set sp_employer_name = b.employer_name,
			sp_job_title = b.job_title,
			sp_prefix = b.prefix
		from RW.eplus_extract_CJD_1 a join
		     advrdb.uw_bio_extract b on (a.sp_id_number = b.id_number and
			                             isnull(a.sp_id_number,'') <> '')

--print 'UpdateProspect info (' + cast(getdate() as nvarchar) + ')'	

	   --Prospect information
	   update a
	   set prospect_id = b.prospect_id
          ,prospect_name = b.prospect_name
          ,prospect_name_sort = b.prospect_name_sort
          ,prospect_region_code = b.region_code
          ,prospect_region_code_desc = b.REGION_CODE_DESC
		,prospect_region_code_sub = b.region_code_sub
          ,prospect_region_code_sub_desc = b.region_code_sub_desc
          ,rating_description = b.RATING_DESCRIPTION
		  ,programs = b.programs
          ,uw_contacter_name = b.CONTACTER_NAME
          ,uw_fundraisers_assigned = b.FUNDRAISERS_ASSIGNED
          --,uw_group_description = b.GROUP_DESCRIPTION HSS
          ,uw_last_contact_date = b.LAST_CONTACT_DATE
          ,uw_last_contact_desc = b.last_contact_desc
          ,uw_last_contact_type_desc = b.last_contact_type_desc
          ,uw_last_personal_contact_date = b.last_personal_contact_date
          ,uw_last_personal_contact_desc = b.last_personal_contact_desc
          ,uw_last_personal_contacter_name = b.last_personal_contacter_name
          ,uw_last_sig_contact_date = b.LAST_SIG_CONTACT_DATE
          ,uw_last_sig_contact_desc = b.last_sig_contact_desc
          ,uw_last_sig_contact_name = b.LAST_SIG_CONTACTER_NAME
          ,uw_last_sig_contact_type_desc = b.LAST_SIG_CONTACT_TYPE_DESC
		  ,uw_classification_desc = b.CLASSIFICATION_DESCRIPTION
		  ,HENRY_SUZZALLO_SOCIETY = b.HENRY_SUZZALLO_SOCIETY
		  ,HENRY_SUZZALLO_SOCIETY_desc = b.HENRY_SUZZALLO_SOCIETY_desc
          ,major_prospect_description = b.major_prospect_description
       from RW.eplus_extract_CJD_1 a join 
            advrdb.UW_PROSPECT_EXTRACT b on (a.id_number = b.id_number)

       --Open Pledge amount
  --     update a
	 --   set   a.uw_open_pledge_total = b.open_pledge_total
		--from RW.eplus_extract_CJD_1 a join
		--	 advrdb.UW_GIFT_BY_UNIT_EXTRACT b on (a.id_number = b.id_number and 
		--	                                      b.unit = 1)

--print 'Update Alerts (' + cast(getdate() as nvarchar) + ')'	
        -- Alert messages
	 	update a
	    set a.alert = left(replace(comment,':  |','|'), len(replace(comment,':  |','|')) -1)
	    from RW.eplus_extract_CJD_1 a
		join ( select id_number
				     ,STUFF((SELECT  '|' + message +  ': ' + xcomment
					          FROM  advdb.zz_alert_message
			                  where id_number = y.id_number
						   	and active_ind = 'Y'
			                  FOR XML PATH('')), 1, 1, '') AS comment
			     from RW.eplus_extract_CJD_1 y
		       ) b on a.id_number = b.id_number

	   update a
	    set a.sp_alert =left(replace(comment,':  |','|'), len(replace(comment,':  |','|')) -1)
	    from RW.eplus_extract_CJD_1 a
		join ( select id_number
				     ,STUFF((SELECT  '|' + message +  ': ' + xcomment
					          FROM  advdb.zz_alert_message
			                  where id_number = isnull(y.sp_id_number,'')
						     and active_ind = 'Y'
			                  FOR XML PATH('')), 1, 1, '') AS comment
			     from RW.eplus_extract_CJD_1 y
		       ) b on (isnull(a.sp_id_number, '')  = b.id_number)

--print 'Unit Exclusions (' + cast(getdate() as nvarchar) + ')'	

     --Build Temp Table with child units
     select id, rw.fn_tx_GetChildNodes(id)[children]
     into #childNodes
     from advrdb.UW_TAXONOMY_EXTRACT

	--"Receive Nothing From..."
	update a
     --If we're just doing a substring check, we probably don't need to do anything fancy in terms of string formatting
	set a.unit_excl = isnull(c1.children,'') + '|' + isnull(c2.children,'') + '|' + isnull(c3.children,'')
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ENTITY en on a.id_number = en.ID_NUMBER and (isnull(en.MAIL_CODE6,'') <> '' or isnull(en.MAIL_CODE7,'') <> '' or isnull(en.MAIL_CODE8,'') <> '')
          left join #childNodes c1 on en.MAIL_CODE6 = c1.id
          left join #childNodes c2 on en.MAIL_CODE7 = c2.id
          left join #childNodes c3 on en.MAIL_CODE8 = c3.id
	--"Receive NO SOLICITATIONS from"	
	update a
	set a.unit_excl_solicit = isnull(c1.children,'') + '|' + isnull(c2.children,'')
	from RW.eplus_extract_CJD_1 a 
        join ADVDB.ENTITY en on a.id_number = en.ID_NUMBER and (isnull(en.MAIL_CODE9,'') <> '' or isnull(en.MAIL_CODE10,'') <> '')
        left join #childNodes c1 on c1.id = en.mail_code9
        left join #childNodes c2 on c2.id = en.mail_code10

--print 'Unit Inclusions (' + cast(getdate() as nvarchar) + ')'	

	--"Receive ONLY from"	
	update a
	set a.unit_incl = isnull(c1.children,'') + '|' + isnull(c2.children,'')
	from RW.eplus_extract_CJD_1 a
		join ADVDB.ENTITY en on a.id_number = en.ID_NUMBER and (isnull(en.MAIL_CODE4,'') <> '' or isnull(en.MAIL_CODE5,'') <> '')
          left join #childNodes c1 on c1.id = en.mail_code4
          left join #childNodes c2 on c2.id = en.mail_code5

	/**************************************************************************************************
		Build and concatenate all of the unit choices (with affil levels) that a person could pick
		I've included the actual unit affiliation (could be school, could be dept, etc.)
		I've also included the school parent of those depts if they are in the person's list 
		already.
		aki 2016-03-28
		2:51
	**************************************************************************************************/

--print 'Unit affiliations (' + cast(getdate() as nvarchar) + ')'	
	--Consistantly faster to skip index creation. 
	--IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('ADVDB.AFFILIATION') AND NAME in ('eplus_x_affilCode'))
	--	DROP INDEX eplus_x_affilCode ON ADVDB.AFFILIATION
 --    CREATE NONCLUSTERED INDEX eplus_x_affilCode ON [ADVDB].[AFFILIATION] ([AFFIL_LEVEL_CODE])
 --         INCLUDE ([ID_NUMBER],[AFFIL_CODE],[RECORD_TYPE_CODE])

	IF EXISTS (SELECT * FROM sysobjects
				WHERE id = object_id(N'RWTEMP.affilTemp_CJD')
				  AND OBJECTPROPERTY(id, N'IsTable') = 1)
	DROP TABLE RWTEMP.affilTemp_CJD

	IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'RWTEMP.affilTemp2') AND OBJECTPROPERTY(id, N'IsTable') = 1)	
		DROP TABLE RWTEMP.affilTemp2
	CREATE TABLE RWTEMP.affilTemp2 (id int, parent int)
	CREATE UNIQUE INDEX tax_id ON RWTEMP.affilTemp2 (id)

	INSERT INTO RWTEMP.affilTemp2 (id, parent)
	SELECT id, RG.fn_getTaxonomyParentID(id,2) parent 
	FROM ADVRDB.UW_Taxonomy

	-- Concatenate the affil unit and the level codes	
	--24s
	select
	  id_number
	, cast(affil_code as nvarchar) + affil_level_code + ',' + cast(parent_affil_code as nvarchar) + affil_level_code as affil_code
	into RWTEMP.affilTemp_CJD
	from (
		-- Pick only the highest level for each unique affil unit
			-- Order the affil units by the actual affil_code sort orders in TMS
		  select 
			 id_number
		  , affil_code
            , parent_affil_code
		  , affil_level_code
		  , ROW_NUMBER() over(partition by id_number, affil_code order by adv_order) as seq
		  from (
			 -- Put the parent affiliation units and actual affiliations units together in a single table
			 select  --distinct
			     a.id_number
                , a.AFFIL_CODE
			 , b.parent parent_affil_code
			 , a.affil_level_code affil_level_code
			 , zz.ADV_ORDER ADV_ORDER --select top 100 *
			 from ADVDB.AFFILIATION a
				    join RW.eplus_extract_CJD_1 y on a.id_number = y.id_number
					   --and y.id_number = '0000000001'
				    join RWTEMP.affilTemp2 b ON b.id = a.AFFIL_CODE 
				    left join ADVDB.ZZ_ADV_TABLE zz on a.AFFIL_LEVEL_CODE = zz.ADV_TABLE_CODE
					   and zz.ADV_TABLE_TYPE = 'A7'
			 where a.AFFIL_LEVEL_CODE <> 'C'
		  ) x
	) xx
     where seq = 1
	
	DROP TABLE RWTEMP.affilTemp2

	--DROP INDEX eplus_x_affilCode ON ADVDB.AFFILIATION
	--create unique index epaffils_x_id_unit on RWTEMP.affilTemp_CJD(id_number, affil_code)

	--1:02(Takes about 1:31 on my machine, can be reduced to around 50 seconds by combining child and parent units)
	update a
	set a.affils = b.affils --select *
	from RW.eplus_extract_CJD_1 a
		join (
			select id_number
				,STUFF(
					(SELECT  ',' + AFFIL_CODE
					 FROM  RWTEMP.affilTemp_CJD
			           where id_number = y.id_number
			           FOR XML PATH('')), 1, 1, '') AS affils
			from RW.eplus_extract_CJD_1 y
			--order by id_number, affils
		) b on a.id_number = b.id_number

     update a
	  set run_date = getdate()
       from  RW.eplus_extract_CJD_1 a;

	IF OBJECT_ID ('RW.eplus_extract_CJD_1') IS NOT NULL --BEGIN
		DROP TABLE RW.eplus_extract_CJD
		EXEC sp_rename 'RW.eplus_extract_CJD_1', 'eplus_extract_CJD'
	--END

	--IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('RW.eplus_extract') AND NAME in ('eplus_x_id_number'))
	--	DROP INDEX eplus_x_id_number ON RW.eplus_extract
	--	CREATE UNIQUE INDEX eplus_x_id_number on RW.eplus_extract(id_number,fg_id_number)
	--		include (no_mail, no_mail_solicit, no_email, no_email_solicit, no_phone, no_phone_solicit)

	--IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('RW.eplus_extract') AND NAME in ('eplus_x_IdFgJntAffExcl'))
	--	DROP INDEX eplus_x_IdFgJntAffExcl ON RW.eplus_extract
	--	CREATE NONCLUSTERED INDEX eplus_x_IdFgJntAffExcl ON [RW].[eplus_extract] ([fg_id_number])
	--		INCLUDE ([id_number],[jnt_mailings_ind],[affils],[unit_excl])--[RW].[eplus_extract_427])
END






GO



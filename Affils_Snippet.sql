IF EXISTS (SELECT * FROM sysobjects
				WHERE id = object_id(N'RWTEMP.affilTemp_CJD')
				  AND OBJECTPROPERTY(id, N'IsTable') = 1)
	DROP TABLE RWTEMP.affilTemp_CJD

	IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'RWTEMP.affilTemp2') AND OBJECTPROPERTY(id, N'IsTable') = 1)	
		DROP TABLE RWTEMP.affilTemp2
	CREATE TABLE RWTEMP.affilTemp2 (id int, parent int)
	CREATE UNIQUE INDEX tax_id ON RWTEMP.affilTemp2 (id)

	INSERT INTO RWTEMP.affilTemp2 (id, parent)
	SELECT id, RG.fn_getTaxonomyParentID(id,2) parent 
	FROM ADVRDB.UW_Taxonomy

	-- Concatenate the affil unit and the level codes	
	--24s
	select
	  id_number
	, cast(affil_code as nvarchar) + affil_level_code + ',' + cast(parent_affil_code as nvarchar) + affil_level_code as affil_code
	into RWTEMP.affilTemp_CJD
	from (
		-- Pick only the highest level for each unique affil unit
		select
			id_number
		, affil_code
          , parent_affil_code
		, affil_level_code
		from (
			-- Order the affil units by the actual affil_code sort orders in TMS
			select 
				id_number
			, affil_code
               , parent_affil_code
			, affil_level_code
			, ROW_NUMBER() over(partition by id_number, affil_code order by adv_order) as seq
			from (
				-- Put the parent affiliation units and actual affiliations units together in a single table
				select  --distinct
			       a.id_number
                    , a.AFFIL_CODE
				, b.parent parent_affil_code
				, a.affil_level_code affil_level_code
				, zz.ADV_ORDER ADV_ORDER --select top 100 *
				from ADVDB.AFFILIATION a
					join RW.eplus_extract_CJD_1 y on a.id_number = y.id_number
						--and y.id_number = '0000000001'
					join RWTEMP.affilTemp2 b ON b.id = a.AFFIL_CODE 
					left join ADVDB.ZZ_ADV_TABLE zz on a.AFFIL_LEVEL_CODE = zz.ADV_TABLE_CODE
						and zz.ADV_TABLE_TYPE = 'A7'
				where a.AFFIL_LEVEL_CODE <> 'C'
			) x
		) xx
		where seq = 1
	) xxx
	
	DROP TABLE RWTEMP.affilTemp2

     select top 1000 * from RWTEMP.affilTemp_CJD order by ID_NUMBER, affil_code
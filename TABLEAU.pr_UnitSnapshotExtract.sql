USE [adv_data]
GO

/****** Object:  StoredProcedure [TABLEAU].[pr_UnitSnapshotExtract]    Script Date: 1/17/2018 3:44:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Evie Le
-- Create date:	2015-12-23
-- Description:	Tableau extract for the Unit Snapshot Report dashboard.
--				Gets all alumni and donors.
--				Monthly totals down to School --> Division --> Department level.
-- Approx. run:	10m 45s
-- =============================================


/************************************************************************************
    MODIFICATION HISTORY
*************************************************************************************
Alex Ichinoe		2016-04-12		Moving this to the TABLEAU schema and adding it to the build wrapper
Evie Le				2016-05-27		Add state name
Evie Le				2016-06-08		Changed alumni definition (anyone with AL record type from ADVDB.ENTITY_RECORD_TYPE)
									Changed donor definition (anyone with credit > 0 from ADVRDB.UW_GIFT_PMT)

Evie Le				2017-01-06		Added degree_schools so that units can see where their alumni give to.
									Commented out unused fields.

Evie Le				2017-05-26		Comment out more unused fields. 
									Minor changes for improving performance (13m --> 10m45s)

Carter Drew			2018-01-22		Further performance tuning

************************************************************************************/


/************************************************************************************
	HERE WE GO!
************************************************************************************/
ALTER PROCEDURE [TABLEAU].[pr_UnitSnapshotExtract_CJD] @full_restore bit

AS
BEGIN

SET NOCOUNT ON;

declare @reachback int

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Begin Sproc', 'Procedure Event', GETDATE())

if @full_restore = 1
begin
    IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TABLEAU].[UnitSnapshotExtract_CJD]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
    begin
	    DROP TABLE TABLEAU.UnitSnapshotExtract_CJD
    end
    set @reachback = 1900
    CREATE TABLE TABLEAU.UnitSnapshotExtract_CJD (

	--ENTITY INFO
	[id_number] [char](10),
	[fg_id_number] [char](10) NULL,
	[spouse_id_number] [char](10) NULL,
	[report_name] [nvarchar](60) NULL,
	[record_type_code] [varchar](2) NULL,
	--[record_type_desc] [varchar](40) NULL,
	[record_status_code] [varchar](2) NULL,
	--[record_status_desc] [varchar](40) NULL,
	[age] [varchar](3) NULL,

	--AFFIL
	[affil_code] [varchar](5),
	--[affil_name] [varchar](255),
	[alumni_donor] [varchar](1),
	[class_year] [nvarchar](4) NULL,	
	[degree_level] [nchar](1) NULL,
	[degree_level_desc] [varchar](15) NULL,
	[donor_type] [varchar](16) NULL,
	
	--ADDRESS
	[city_current] [varchar](50) NULL,
	--[state_code_current] [varchar](3) NULL,
	[state_name_current] [varchar](40) NULL,
	[zipcode_current] [varchar](30) NULL,
	--[county_code_current] [varchar](5) NULL,
	[county_name_current] [varchar](255) NULL,
	--[country_code_current] [varchar](5) NULL,
	[country_name_current] [varchar](255) NULL,
	--[region_code] [varchar](5) NULL,
	[region_desc] [varchar](40) NULL,
	--[sub_region_code] [varchar](5) NULL,
	[sub_region_desc] [varchar](40) NULL,
	--[latitude_current] [decimal] (9,6) NULL,
	--[longitude_current] [decimal] (9,6) NULL,

	----EMPLOYMENT
	--[employer_name] [varchar](60) NULL,
	--[job_title] [varchar](60) NULL,

	----UWAA
	--[UWAA_member_type_code] [varchar](10) null,
 --    [UWAA_member_type_desc] [varchar](40) null,
 --    --[UWAA_member_status_code] [varchar](10) null,
 --    [UWAA_member_status_desc] [varchar](35) null,
 --    [UWAA_exp_date] date null,

	--GIFT
	--[allocation_code] [varchar](16) NULL,
	--[allocation_name] [varchar](40) NULL,
	[department_code] [varchar](20) NULL,
	[department_name] [varchar](40) NULL,
	[division_code] [varchar](20) NULL,
	[division_name] [varchar](40) NULL,
	[school_code] [varchar](4) NULL,
	[school_name] [varchar](60) NULL,
	--[campus_code] [varchar](4) NULL,
	--[campus_name] [varchar](10) NULL,

	--RFM/ENGAGEMENT
	[engagement_score] [numeric](6) NULL,
	[UW_RFM] [numeric](10) NULL,
	[affil_RFM] [numeric](10) NULL,

	--GIFT
	[month_of_record] [int] NULL,
	[fy_month_of_record] [int] NULL,
	--[calendar_year] [char](4) NULL,
	[fiscal_year] [char](4) NULL,
	[legal_amount] [money] NULL ,

	degree_schools varchar(200),

	runtime datetime null)
end
else
begin
    set @reachback = 2010
    delete 
    from TABLEAU.UnitSnapshotExtract_CJD
    where fiscal_year >= @reachback or fiscal_year is null
end

/************************************************************************************
	Create table
************************************************************************************/

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Create Table', 'Procedure Event', GETDATE())

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Insert Donors', 'Procedure Event', GETDATE())

/************************************************************************************
	Insert donors & monthly dept totals from UW_GIFT_PMT
************************************************************************************/
INSERT INTO TABLEAU.UnitSnapshotExtract_CJD (
	id_number
  , affil_code
  --, affil_name
  , alumni_donor
  , department_code
  , department_name
  , division_code
  , school_code
  , month_of_record
  --, calendar_year
  , fiscal_year
  , legal_amount
  , fy_month_of_record
)
SELECT
	g.id_number
  , t.taxonomy_id
  --, t.code_desc
  , 'D'
  , DEPARTMENT
  , t.code_desc
  , DIVISION
  , school
  , MONTH_OF_RECORD
  --, LEFT(date_of_record, 4)
  , YEAR_OF_GIVING
  , SUM(legal_amount) AS legal_amount
  , CASE WHEN month_of_record >= 7 THEN month_of_record - 6 ELSE month_of_record + 6 END	--fy_month_of_record
FROM ADVRDB.UW_GIFT_PMT g
JOIN ADVRDB.UW_TAXONOMY_CODE_CATALOG_EXTRACT t ON t.code = g.department
	AND code_type = 'J5'
WHERE CREDIT_AMOUNT > 0
	AND EXISTS (SELECT 'x' FROM ADVDB.ENTITY e WHERE e.ID_NUMBER = g.ID_NUMBER AND e.RECORD_STATUS_CODE IN ('A','L','R','D'))
     AND YEAR_OF_GIVING >= @reachback
GROUP BY 
  g.id_number
, taxonomy_id
--, code_desc
, DEPARTMENT
, code_desc
, DIVISION
, school
, MONTH_OF_RECORD
--, LEFT(date_of_record,4)
, YEAR_OF_GIVING
--join 2:18
--exists 37s

/************************************************************************************
	Insert alumni from ADVDB.DEGREES
	Include all degree types, even Certificate, Residency, Fellowship, etc.
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Insert Alumni', 'Procedure Event', GETDATE())
	
INSERT INTO TABLEAU.UnitSnapshotExtract_CJD (
	id_number
  , affil_code
  --, affil_name
  , alumni_donor
  , class_year
  , degree_level
  , department_code
  , department_name
  , school_code
  , school_name
  --, campus_code
  --, campus_name
)
SELECT DISTINCT
	d.ID_NUMBER
  , t.taxonomy_id
  --, t.code_desc
  , 'A'
  , d.DEGREE_YEAR
  , d.DEGREE_LEVEL_CODE
  , d.DEPT_CODE
  , t.code_desc
  , d.SCHOOL_CODE
  , z.ADV_SHORT_DESC
 -- , d.CAMPUS_CODE --s.pref_class_year
 -- , CASE d.campus_code
	--	WHEN '0' THEN 'Seattle'
	--	WHEN '1' THEN 'Bothell'
	--	WHEN '2' THEN 'Tacoma'
	--END
FROM ADVDB.DEGREES d
LEFT JOIN ADVDB.ZZ_ADV_TABLE z ON z.ADV_TABLE_CODE = d.SCHOOL_CODE
	AND ADV_TABLE_TYPE = 'AF'
LEFT JOIN ADVRDB.UW_TAXONOMY_CODE_CATALOG_EXTRACT t ON t.code = d.DEPT_CODE
	AND code_type = 'EJ'
WHERE EXISTS (SELECT 'x' FROM ADVDB.ENTITY_RECORD_TYPE r WHERE r.ID_NUMBER = d.ID_NUMBER AND RECORD_TYPE_CODE = 'AL') 
	AND EXISTS (SELECT 'x' FROM ADVDB.ENTITY e WHERE e.ID_NUMBER = d.ID_NUMBER AND e.RECORD_STATUS_CODE IN ('A','L','R','D'))
--4

/************************************************************************************
	Update bio & addresses
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update Bio Data', 'Procedure Event', GETDATE())

UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET fg_id_number = b.fg_id_number,
    spouse_id_number = b.spouse_id_number,
    report_name = b.report_name,
    record_status_code = b.ENTITY_STATUS,
	record_type_code = b.ENTITY_RECORD_TYPE,
    --job_title = b.JOB_TITLE,
    --employer_name = b.EMPLOYER_NAME,
    engagement_score = b.ENGAGEMENT_SCORE_HOUSE
FROM TABLEAU.UnitSnapshotExtract_CJD a
JOIN ADVRDB.UW_BIO_EXTRACT b ON b.id_number = a.id_number
WHERE a.fiscal_year >= @reachback or a.fiscal_year is null
--29s

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update Address Data', 'Procedure Event', GETDATE())

UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET city_current = c.CITY
  --, state_code_current = c.STATE_CODE
  , state_name_current = c.STATE_CODE_DESC
  , zipcode_current = c.ZIPCODE
  --, county_code_current = c.COUNTY_CODE
  , county_name_current = c.COUNTY_CODE_DESC
  --, country_code_current = c.COUNTRY_CODE
  , country_name_current = c.COUNTRY_CODE_DESC
  --, region_code = c.REGION_CODE
  , region_desc = c.REGION_CODE_DESC
  --, sub_region_code = c.REGION_CODE_SUB
  , sub_region_desc = c.REGION_CODE_SUB_DESC
FROM TABLEAU.UnitSnapshotExtract_CJD a
JOIN ADVRDB.UW_ADDRESS c ON c.ID_NUMBER = a.id_number
    AND ADDR_PREF_IND = 'Y'
WHERE a.fiscal_year >= @reachback or a.fiscal_year is null

/************************************************************************************
	Update from ZZ_ADV_TABLE
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update TMS Values', 'Procedure Event', GETDATE())

UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET degree_level_desc = z.ADV_SHORT_DESC --select *
FROM TABLEAU.UnitSnapshotExtract_CJD a
JOIN ADVDB.ZZ_ADV_TABLE z ON z.ADV_TABLE_CODE = a.degree_level
	AND z.ADV_TABLE_TYPE = 'D7'
	AND z.ADV_STATUS_CODE = 'A'
WHERE a.fiscal_year >= @reachback or a.fiscal_year is null

--UPDATE TABLEAU.UnitSnapshotExtract_CJD
--SET record_type_desc = zz.adv_short_desc
--FROM TABLEAU.UnitSnapshotExtract_CJD ede
--JOIN ADVDB.ZZ_ADV_TABLE zz ON ede.record_type_code = zz.ADV_TABLE_CODE
--WHERE zz.ADV_TABLE_TYPE = 'AC'
  
--UPDATE TABLEAU.UnitSnapshotExtract_CJD
--SET record_status_desc = zz.adv_short_desc
--FROM TABLEAU.UnitSnapshotExtract_CJD ede
--JOIN ADVDB.ZZ_ADV_TABLE zz ON ede.record_status_code = zz.ADV_TABLE_CODE
--WHERE zz.ADV_TABLE_TYPE = 'AB'

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update School Names', 'Procedure Event', GETDATE())

--Make allocation school match degree school name
UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET school_name = CASE
				  WHEN school_code IN ('ARCH','AS','ENV','BUS','EDUC','ENG','PUBH','NURS','PHAR','PUBA','GRAD','GS','SOCW','DENT','BOT','LAW','MED','TAC') THEN z1.ADV_SHORT_DESC
				  ELSE z2.ADV_SHORT_DESC
			   END
FROM TABLEAU.UnitSnapshotExtract_CJD ad
LEFT JOIN ADVDB.ZZ_ADV_TABLE z1 ON ad.school_code = z1.ADV_VERY_SHORT_DESC --school
    AND z1.ADV_TABLE_TYPE = 'AF'
    AND z1.ADV_STATUS_CODE = 'A'
LEFT JOIN ADVDB.ZZ_ADV_TABLE z2 ON ad.school_code = z2.ADV_TABLE_CODE --allocation school
    AND z2.ADV_TABLE_TYPE = 'J2'
WHERE alumni_donor = 'D' and (ad.fiscal_year >= @reachback or ad.fiscal_year is null)

     
UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET division_name = ADV_SHORT_DESC
FROM TABLEAU.UnitSnapshotExtract_CJD ad
JOIN advdb.ZZ_ADV_TABLE zz ON ad.division_code = zz.ADV_TABLE_CODE
WHERE zz.ADV_TABLE_TYPE = 'J6' and (ad.fiscal_year >= @reachback or ad.fiscal_year is null)

/************************************************************************************
	Update age from ag.solicit_extract
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update AG Age', 'Procedure Event', GETDATE())

UPDATE ede
SET age = se.age
FROM TABLEAU.UnitSnapshotExtract_CJD ede
JOIN AG.solicit_extract se ON ede.id_number = se.id_number
WHERE ede.fiscal_year >= @reachback or ede.fiscal_year is null
--49s

/************************************************************************************
	Update RFM based on unit
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update RFM', 'Procedure Event', GETDATE())
--EDIT MADE: Combined Update Statements
--Broken, UW RFM left blank if affil RFM is blank
UPDATE ede
SET UW_RFM = bUW.RFM,
    affil_RFM = bUnit.RFM
FROM TABLEAU.UnitSnapshotExtract_CJD ede
left JOIN ADVRDB.UW_GIFT_BY_UNIT_EXTRACT_INDIVIDUAL bUW ON bUW.ID_NUMBER = ede.id_number
	AND bUW.UNIT = 1
left JOIN ADVRDB.UW_GIFT_BY_UNIT_EXTRACT_INDIVIDUAL bUnit ON bUnit.ID_NUMBER = ede.id_number
	AND bUnit.UNIT = ede.affil_code
WHERE ede.fiscal_year >= @reachback or ede.fiscal_year is null

/************************************************************************************
	Update UWAA Membership

INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'Unit Snapshot-Update UWAA Memberships', 'Procedure Event', GETDATE())

UPDATE a
SET UWAA_member_type_code = b.MEMB_TYPE_CODE,
    UWAA_member_type_desc = b.MEMB_TYPE_DESC,
    --UWAA_member_status_code = b.MEMB_CURRENT_STATUS_CODE,
    UWAA_member_status_desc = b.MEMB_STATUS_DESCRIPTION,
    UWAA_exp_date = b.MEMB_DATE_EXPIRED --select *
FROM TABLEAU.UnitSnapshotExtract_CJD a
JOIN ADVRDB.UW_MEMBERSHIP_EXTRACT_ALL b ON b.ID_NUMBER = a.id_number
    AND MEMB_UNIT_CODE = 'UA'
JOIN ADVDB.ZZ_ADV_TABLE z ON z.ADV_TABLE_CODE = b.MEMB_CURRENT_STATUS_CODE
    AND z.ADV_TABLE_TYPE = 'MS'
--0:21
************************************************************************************/

/************************************************************************************
	Update donor_type based on school
		Alumni Non-Donor
		Non-Alumni Donor
		Alumni Donor
************************************************************************************/
INSERT INTO RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) VALUES 
	('Tableau', 'Unit Snapshot-Update Donor Type Flag', 'Procedure Event', GETDATE());

--EDIT MADE: Replaced temp table with CTE and Case statements

with alumniDonors as(
    SELECT id_number, school_name, COUNT(DISTINCT alumni_donor) AS alumni_donor
    FROM TABLEAU.UnitSnapshotExtract_CJD
    WHERE fiscal_year >= @reachback or fiscal_year is null
    GROUP BY id_number, school_name
    HAVING COUNT(DISTINCT alumni_donor) > 1
)
UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET donor_type = case
                    when a.id_number is not null then 'Alumni Donor'
                    when ede.alumni_donor = 'A' then 'Alumni Non-Donor'
                    when ede.alumni_donor = 'D' then 'Non-Alumni Donor'
                 end
FROM TABLEAU.UnitSnapshotExtract_CJD ede
left join alumniDonors a on a.id_number = ede.id_number and a.school_name = ede.school_name
WHERE ede.fiscal_year >= @reachback or ede.fiscal_year is null
--0:42

/************************************************************************************
	Get each school they are alumni of and concat 
	so that schools can find where their alumni give to.
************************************************************************************/
INSERT into RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) values 
	('Tableau', 'TPC Snapshot-Update Affiliation', 'Procedure Event', GETDATE())

SELECT distinct id_number, school_name
INTO RWTEMP.UnitSnapshotExtract_1_alumni
FROM TABLEAU.UnitSnapshotExtract_CJD
WHERE alumni_donor = 'A'
and fiscal_year >= @reachback or fiscal_year is null

SELECT
	id_number
  , deg_schools = STUFF( (SELECT ' | ' + school_name
						  FROM RWTEMP.UnitSnapshotExtract_1_alumni AS a2
						  WHERE ID_NUMBER = a.ID_NUMBER
						  ORDER BY school_name
						  FOR XML PATH(''), TYPE).value ('.', 'varchar(max)'), 1, 1, '')
INTO RWTEMP.UnitSnapshotExtract_1_alumnischools
FROM RWTEMP.UnitSnapshotExtract_1_alumni AS a
GROUP BY ID_NUMBER

CREATE INDEX idx1 ON RWTEMP.UnitSnapshotExtract_1_alumnischools (id_number)

UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET degree_schools = LTRIM(b.deg_schools)
FROM TABLEAU.UnitSnapshotExtract_CJD a
JOIN RWTEMP.UnitSnapshotExtract_1_alumnischools b ON b.id_number = a.id_number
where a.fiscal_year >= @reachback or a.fiscal_year is null


DROP TABLE RWTEMP.UnitSnapshotExtract_1_alumni
DROP TABLE RWTEMP.UnitSnapshotExtract_1_alumnischools

/************************************************************************************
	Create final table and indexes
************************************************************************************/

--CREATE INDEX UnitSnapshotExtract_key1 ON TABLEAU.UnitSnapshotExtract_CJD (id_number)
--CREATE INDEX UnitSnapshotExtract_key2 ON TABLEAU.UnitSnapshotExtract_CJD (fg_id_number)

UPDATE TABLEAU.UnitSnapshotExtract_CJD
SET runtime = GETDATE()

INSERT INTO RG.REPORTING_EXTRACT_BUILD_LOG (log_level, checkpoint_desc, checkpoint_type, runtime) VALUES 
	('Tableau', 'Unit Snapshot-Sproc Complete', 'Procedure Event', GETDATE())

/************************************************************************************
    END
************************************************************************************/
END 

--SELECT TOP 100 * FROM TABLEAU.UnitSnapshotExtract_CJD WHERE alumni_donor = 'A'


GO

--TEST CODE
--select *
--from TABLEAU.UnitSnapshotExtract
--where id_number = 0000142582
--order by school_name

--select *
--from TABLEAU.UnitSnapshotExtract_CJD
--where id_number = 0000142582
--order by school_name

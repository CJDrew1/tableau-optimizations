/*=============================================
-- Author:		Carter Drew
-- Create date: 2018-01-31
-- Description: Keep a persistant log of users who have been updated within the given number of days 
=============================================*/

alter proc RW.pr_build_updated_IDs @reachback int = 2 as
begin
    IF OBJECT_ID('RW.updated_IDs', 'U') IS NOT NULL 
      DROP TABLE RW.updated_IDs; 

    create table RW.updated_IDs (ID_NUMBER nvarchar(10), TABLE_NAME nvarchar(100), DATE_MODIFIED datetime)

    insert into rw.updated_IDs
    select ID_NUMBER, 'ENTITY', DATE_MODIFIED
    from advdb.ENTITY
    where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())
    union
    select ID_NUMBER, 'UW_GIFT_PMT', DATE_MODIFIED
    from advrdb.UW_GIFT_PMT
    where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())
    union
    select ID_NUMBER, 'ADDRESS', DATE_MODIFIED
    from advdb.ADDRESS
    where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())
    union
    select ID_NUMBER, 'DEGREES', DATE_MODIFIED
    from advdb.DEGREES
    where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())
    union
    select ID_NUMBER, 'EMPLOYMENT', DATE_MODIFIED
    from advdb.EMPLOYMENT
    where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())
    --2/5/2018
    --Removed Affiliation scan. 
    --union
    --select ID_NUMBER, 'AFFILIATION', DATE_MODIFIED
    --from advdb.AFFILIATION
    --where DATE_MODIFIED >= DATEADD(day, -@reachback, getdate())

    --RELEVANT AFFIL TABLES
    --Degree, membership, gift_pmt, employment, 
end